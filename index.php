<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Lucas</title>
    <link rel="icon" href="image.png">
    <style>
      @import url("https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css");
      body {
        text-align: center;
        font-family: sans-serif;
      }
      footer {
        font-family: monospace;
      }
    </style>
  </head>
  <body>
    <header>
      <img width="100px" height="auto" src="image.png" alt="Image"><br>
      <h1>Lucas</h1>
      <p>(a.k.a. "Absucc")</p>
      <a href="?">Home</a> - <a href="?projects">Projects</a> - <a href="?contact">Contact</a> - <a href="?about">About web</a> - <a href="http://ctrl-c.club/~lucas">~</a>
    </header>
    <?php if(isset($_GET["projects"])) { ?>
      <div class="section" id="projects">
        <h2>Projects</h2>
        <h3>Programming</h3>
        <p><a href="https://apicord.rf.gd">APIcord</a><br>Bitamodern<br>Codename UpRuffle (Coming soon)<br><a href="https://getiton.codeberg.page">GetItOn.Codeberg.page</a><br><a href="https://api.l64.repl.co">Lucas' APIs of awesome</a><br><a href="https://absucc.github.io/pass-protect">pass-protect</a><br><a href="https://absucc.github.io/redefault">Redefault.css</a><br><a href="https://xkcd.l64.repl.co">XKmodern</a></p>
        <h3>Memes</h3>
        <p><a href="https://eposting.herokuapp.com">Eposting</a></p>
      </div>
    <?php } elseif (isset($_GET["contact"])) { ?>
      <div class="section" id="contact">
        <h2>Contact me on</h2>
        <p><b>Discord:</b> error#7900<br><b>Telegram:</b> @webLucas <a href="https://t.me/webLucas"><button>Open</button></a><!--<br><b>Twitter:</b> @absucc_ <a href="https://twitter.com/absucc_"><button>Open</button></a>--></p>
      </div>
    <?php } elseif (isset($_GET["about"])) { ?>
      <div class="section" id="about">
        <h2>About this website</h2>
        <p>This webpage was made with <a href="https://html.spec.whatwg.org">HTML</a>, <a href="https://www.php.net">PHP</a> and a little of <a href="https://www.w3.org/TR/CSS/">CSS</a></p>
        <h3>Thanks to</h3>
        <h4>Utilities</h4>
        <li><a href="https://replit.com">Replit</a> (Code editor)</li>
        <h4>Hosting</h4>
        <li><a href="https://replit.com">Replit</a> (<a href="https://replit.com/@L64/L64">@L64/L64</a> & <a href="https://l64.repl.co">website</a>)</li>
        <li><a href="https://codeberg.org">Codeberg</a> (<a href="https://codeberg.org/lucas/website">lucas/website</a>)</li>
      </div>
    <?php } else { ?>
      <!--<h1>Lucas</h1>-->
      <h2>Amateur programmer, lover of the informatic world, FOSS enthusiast & procrastinator</h2>
      <a href="https://github.com/absucc"><i class="fa fa-github"></i></a>
      <a href="https://youtube.com/channel/UC1hxJrwpO-EsMkfje4mF3Xg"><i class="fa fa-youtube-play"></i></a>
      <a href="https://pt.neko.bar/video-channels/lucastv"><i class="fa fa-peertube"></i></a>
      <a href="https://codeberg.org/lucas">Codeberg</a>
      <a href="https://reddit.com/u/L64ok"><i class="fa fa-reddit"></i></a>
      <a rel="me" href="https://mastodon.online/@lucas"><i class="fa fa-mastodon"></i></a>
      <a href="https://twitter.com/absucc_"><i class="fa fa-twitter"></i></a>
      <a href="https://t.me/absucc"><i class="fa fa-telegram"></i></a>
      <a href="https://twitch.tv/absucc"><i class="fa fa-twitch"></i></a>
    <?php } ?>
      <footer><br>by Absucc - 2021 - <a href="https://codeberg.org/lucas/website">Source Code</a></footer>
    </body>
</html>